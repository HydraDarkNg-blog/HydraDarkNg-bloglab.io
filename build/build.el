;;; build --- prueba
;;; Commentary:
;;; Set the package installation directory so that packages aren't stored in the
;;; ~/.emacs.d/elpa path.

(setq user-emacs-directory "../")

(require 'package)
(setq package-user-dir (expand-file-name "./.packages" user-emacs-directory))
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; Initialize the package system
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(package-install 'htmlize)

;; Load the publishing system
(require 'ox)
(require 'ox-publish)
(require 'ox-html)

(setq org-html-style-default nil
      org-html-validation-link nil)

(setq make-backup-files nil
      auto-save-default nil
      org-export-preserve-breaks t)

(defun org-html-export-modeline (info)
  "Export modeline from Emacs to html.
INFO is the info form org buffer."
   (concat
    "\n<div class=\"modeline\">\n"
    "<div class=\"modeline-buffer-info\">\n"
    "<div class=\"modeline-encoding\">U</div>\n"
    "<div class=\"modeline-end-of-line\">:</div>\n"
    "<div class=\"modeline-document-writable\">%</div>"
    "<div class=\"modeline-document-modified\">%</div>"
    "<div class=\"modeline-document-current-directory\">%</div>"
    "</div>\n"
    "<div class=\"modeline-title\">"
    (when (plist-get info :with-title)
     (let ((title (and (plist-get info :with-title)
                       (plist-get info :title))))
       (when title
         (format
          "<p>%s.html</p>\n"
          (org-export-data title info)))))
    "</div>\n"
    "<div class=\"modeline-space\"></div>"
    "<div class=\"modeline-modes\">"
    "<p>(</p>"
    "<div class=\"modeline-html-mode\">XHTML+</div>"
    "<p>)</p>"
    "\n</div>\n"
    "<div class=\"modeline-space\"></div>"
    "<div class=\"modeline-time\" id=\"modeline-time\"></div>"
    "</div>\n"))

(defun org-html-export-minibuffer ()
  "Export minibuffer."
  (format
   (concat
    "<div class=\"minibuffer\">"
    "</div>")))

(defun org-html--build-pre/postamble (type info)
  "Return document preamble or postamble as a string, or nil.
TYPE is either `preamble' or `postamble', INFO is a plist used as a
communication channel."
  (let ((section (plist-get info (intern (format ":html-%s" type))))
        (spec (org-html-format-spec info)))
    (when section
      (let ((section-contents
             (if (functionp section) (funcall section info)
               (cond
                ((stringp section) (format-spec section spec))
                ((eq section 'auto)
                 (let ((date (cdr (assq ?d spec)))
                       (email (cdr (assq ?e spec)))
                       (creator (cdr (assq ?c spec)))
                       (validation-link (cdr (assq ?v spec))))
                   (concat
                    (and (plist-get info :with-date)
                         (org-string-nw-p date)
                         (format "<p class=\"date\">%s: %s</p>\n"
                                 (org-html--translate "Date" info)
                                 date))
                    (and (plist-get info :with-email)
                         (org-string-nw-p email)
                         (format "<p class=\"email\">%s: %s</p>\n"
                                 (org-html--translate "Email" info)
                                 email))
                    (and (plist-get info :time-stamp-file)
                         (format
                          "<p class=\"date\">%s: %s</p>\n"
                          (org-html--translate "Created" info)
                          (format-time-string
                           (plist-get info :html-metadata-timestamp-format))))
                    (and (plist-get info :with-creator)
                         (org-string-nw-p creator)
                         (format "<p class=\"creator\">%s</p>\n" creator))
                    (and (org-string-nw-p validation-link)
                         (format "<p class=\"validation\">%s</p>\n"
                                 validation-link)))))
                (t
                 (let ((formats (plist-get info (if (eq type 'preamble)
                                                    :html-preamble-format
                                                  :html-postamble-format)))
                       (language (plist-get info :language)))
                   (format-spec
                    (cadr (or (assoc-string language formats t)
                              (assoc-string "en" formats t)))
                    spec)))))))
        (let ((div (assq type (plist-get info :html-divs))))
          (when (org-string-nw-p section-contents)
            (concat
             (format "<%s id=\"%s\" class=\"%s\">\n"
                     (nth 1 div)
                     (nth 2 div)
                     org-html--pre/postamble-class)
             (org-element-normalize-string section-contents)
             (format "</%s>\n" (nth 1 div)))))))))

(defun org-html-template (contents info)
  "Return complete document string after HTML conversion.
CONTENTS is the transcoded contents string.  INFO is a plist
holding export options."
  (concat
   (when (and (not (org-html-html5-p info)) (org-html-xhtml-p info))
     (let* ((xml-declaration (plist-get info :html-xml-declaration))
            (decl (or (and (stringp xml-declaration) xml-declaration)
                      (cdr (assoc (plist-get info :html-extension)
                                  xml-declaration))
                      (cdr (assoc "html" xml-declaration))
                      "")))
       (when (not (or (not decl) (string= "" decl)))
         (format "%s\n"
                 (format decl
                         (or (and org-html-coding-system
                                  ;; FIXME: Use Emacs 22 style here, see `coding-system-get'.
                                  (coding-system-get org-html-coding-system 'mime-charset))
                             "iso-8859-1"))))))
   (org-html-doctype info)
   "\n"
   (concat "<html"
           (cond ((org-html-xhtml-p info)
                  (format
                   " xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"%s\" xml:lang=\"%s\""
                   (plist-get info :language) (plist-get info :language)))
                 ((org-html-html5-p info)
                  (format " lang=\"%s\"" (plist-get info :language))))
           ">\n")
   "<head>\n"
   (org-html--build-meta-info info)
   (org-html--build-head info)
   (org-html--build-mathjax-config info)
   "</head>\n"
   "<body>\n"
   "<div class=\"content-main\">"
   (let ((link-up (org-trim (plist-get info :html-link-up)))
         (link-home (org-trim (plist-get info :html-link-home))))
     (unless (and (string= link-up "") (string= link-home ""))
       (format (plist-get info :html-home/up-format)
               (or link-up link-home)
               (or link-home link-up))))
   ;; Preamble.
   (org-html--build-pre/postamble 'preamble info)
   ;; Document contents.
   (let ((div (assq 'content (plist-get info :html-divs))))
     (format "<%s id=\"%s\" class=\"%s\">\n"
             (nth 1 div)
             (nth 2 div)
             (plist-get info :html-content-class)))
   ;; Document title.
   (when (plist-get info :with-title)
     (let ((title (and (plist-get info :with-title)
                       (plist-get info :title)))
           (subtitle (plist-get info :subtitle))
           (html5-fancy (org-html--html5-fancy-p info)))
       (when title
         (format
          (if html5-fancy
              "<header>\n<h1 class=\"title\">%s</h1>\n%s</header>"
            "<h1 class=\"title\">%s%s</h1>\n")
          (org-export-data title info)
          (if subtitle
              (format
               (if html5-fancy
                   "<p class=\"subtitle\" role=\"doc-subtitle\">%s</p>\n"
                 (concat "\n" (org-html-close-tag "br" nil info) "\n"
                         "<span class=\"subtitle\">%s</span>\n"))
               (org-export-data subtitle info))
            "")))))
   ;; Document title.
   (when (plist-get info :with-author)
     (let ((author (and (plist-get info :with-author)
                       (plist-get info :author)))
           (html5-fancy (org-html--html5-fancy-p info)))
       (when author
         (format
          (if html5-fancy
              "<header>\n<h1 class=\"author\">%s</h1>\n</header>"
            "<h1 class=\"author\">%s</h1>\n")
          (org-export-data author info)))))
   contents
   (format "</%s>\n" (nth 1 (assq 'content (plist-get info :html-divs))))
   ;; Postamble.
   (org-html--build-pre/postamble 'postamble info)
   ;; Possibly use the Klipse library live code blocks.
   "</div>"
   (when (plist-get info :html-klipsify-src)
     (concat "<script>" (plist-get info :html-klipse-selection-script)
             "</script><script src=\""
             org-html-klipse-js
             "\"></script><link rel=\"stylesheet\" type=\"text/css\" href=\""
             org-html-klipse-css "\"/>"))
   (org-html-export-modeline info)
   (org-html-export-minibuffer)
   (when (plist-get info :with-js)
     (let ((js (and (plist-get info :with-js)
                        (plist-get info :js))))
       (when js
         (format
            "<script src=\"%s\"></script>\n"
            (org-export-data js info)))))
   ;; Closing document.
   "</body>\n</html>"))

;; Define the publishing project
(setq org-publish-project-alist
      (list
       (list "dotfiles"
             :base-directory "~/.dotfiles/"
             :publishing-function 'org-html-publish-to-html
             :publishing-directory (expand-file-name "articles/config" user-emacs-directory)
             :base-extension "org"
             :author "HydraDarkNg"
             :with-creator t ;; Include Emacs and Org versions in footer
             :section-numbers nil
             :time-stamp-file nil
             :html-head "<link rel=\"stylesheet\" href=\"../../theme/style.css\">"
             :with-js t
             :js "../../theme/main.js"
             )
       (list "build"
             :base-directory (expand-file-name "build" user-emacs-directory)
             :publishing-function 'org-html-publish-to-html
             :publishing-directory (expand-file-name "articles" user-emacs-directory)
             :author "HydraDarkNg"
             :with-creator t ;; Include Emacs and Org versions in footer
             :section-numbers nil
             :time-stamp-file nil
             :html-head "<link rel=\"stylesheet\" href=\"../theme/style.css\">"
             :with-js t
             :js "../theme/main.js"
             )
       (list "blog"
             :recursive t
             :base-directory (expand-file-name "org" user-emacs-directory)
             :publishing-function 'org-html-publish-to-html
             :publishing-directory (expand-file-name "articles" user-emacs-directory)
             :author "HydraDarkNg"
             :with-creator t ;; Include Emacs and Org versions in footer
             :with-toc t     ;; Include a table of contents
             :section-numbers nil ;; Don't include section numbers
             :time-stamp-file nil
             :html-head "<link rel=\"stylesheet\" href=\"../theme/style.css\">"
             :with-js t
             :js "../theme/main.js")
       (list "index"
             :base-directory user-emacs-directory
             :publishing-function 'org-html-publish-to-html
             :publishing-directory user-emacs-directory
             :author "HydraDarkNg"
             :with-creator t ;; Include Emacs and Org versions in footer
             :with-toc nil
             :section-numbers nil
             :time-stamp-file nil
             :html-head "<link rel=\"stylesheet\" href=\"./theme/style.css\">"
             :with-js t
             :js "./theme/main.js"
             )
       (list "blog-ascii"
             :recursive t
             :base-directory (expand-file-name "org" user-emacs-directory)
             :publishing-function 'org-ascii-publish-to-ascii
             :publishing-directory (expand-file-name "ascii" user-emacs-directory)
             :author "HydraDarkNg"
             :with-creator t ;; Include Emacs and Org versions in footer
             :with-toc t     ;; Include a table of contents
             :section-numbers nil ;; Don't include section numbers
             :time-stamp-file nil)
       (list "build-ascii"
             :recursive t
             :base-directory (expand-file-name "build" user-emacs-directory)
             :publishing-function 'org-ascii-publish-to-ascii
             :publishing-directory (expand-file-name "ascii" user-emacs-directory)
             :author "HydraDarkNg"
             :with-creator t ;; Include Emacs and Org versions in footer
             :with-toc t     ;; Include a table of contents
             :section-numbers nil ;; Don't include section numbers
             :time-stamp-file nil)))

;; Generate the site output
(org-publish-all t)

(message "Build complete!")

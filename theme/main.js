var modelineTitle = document.querySelector(".modeline-title");
var modelineEncoding = document.querySelector(".modeline-encoding");
var modelineEndOfLine = document.querySelector(".modeline-end-of-line");
var modelineDocumentWritable = document.querySelector(".modeline-document-writable");
var modelineDocumentModified = document.querySelector(".modeline-document-modified");
var modelineDocumentCurrentDirectory = document.querySelector(".modeline-document-current-directory");
var modelineHtmlMode = document.querySelector(".modeline-html-mode");
var minibuffer = document.querySelector(".minibuffer");

modelineTitle.addEventListener("mouseover", toogleMinibufferTitle);
modelineTitle.addEventListener("mouseout", toogleMinibuffer);
modelineEncoding.addEventListener("mouseover", toogleMinibufferEncoding);
modelineEncoding.addEventListener("mouseout", toogleMinibuffer);
modelineEndOfLine.addEventListener("mouseover", toogleMinibufferEndOfLine);
modelineEndOfLine.addEventListener("mouseout", toogleMinibuffer);
modelineDocumentWritable.addEventListener("mouseover", toogleMinibufferDocumentWritable);
modelineDocumentWritable.addEventListener("mouseout", toogleMinibuffer);
modelineDocumentModified.addEventListener("mouseover", toogleMinibufferDocumentModified);
modelineDocumentModified.addEventListener("mouseout", toogleMinibuffer);
modelineDocumentCurrentDirectory.addEventListener("mouseover", toogleMinibufferDocumentCurrentDirectory);
modelineDocumentCurrentDirectory.addEventListener("mouseout", toogleMinibuffer);
modelineHtmlMode.addEventListener("mouseover", toogleMinibufferHtmlMode);
modelineHtmlMode.addEventListener("mouseout", toogleMinibuffer);

function toogleMinibufferTitle(){
    var minibuffer = document.querySelector(".minibuffer");
    minibuffer.innerHTML = '<p>Buffer name</p>';
};

function toogleMinibufferEncoding(){
    var minibuffer = document.querySelector(".minibuffer");
    minibuffer.innerHTML = '<p>Buffer coding system (multi-byte): utf-8-unix</p>';
};

function toogleMinibufferEndOfLine(){
    var minibuffer = document.querySelector(".minibuffer");
    minibuffer.innerHTML = '<p>End-of-line style: Unix-style LF</p>';
};

function toogleMinibufferDocumentWritable(){
    var minibuffer = document.querySelector(".minibuffer");
    minibuffer.innerHTML = '<p>Buffer is read-only</p>';
};

function toogleMinibufferDocumentModified(){
    var minibuffer = document.querySelector(".minibuffer");
    minibuffer.innerHTML = '<p>Buffer is not modified</p>';
};

function toogleMinibufferDocumentCurrentDirectory(){
    var minibuffer = document.querySelector(".minibuffer");
    minibuffer.innerHTML = '<p>Current directory is not local</p>';
};

function toogleMinibufferHtmlMode(){
    minibuffer.innerHTML = '<p>Major mode</p>';
};

function toogleMinibuffer(){
    var minibuffer = document.querySelector(".minibuffer");
    minibuffer.innerHTML = '';
};

function startTime()
{
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    // add a zero in front of numbers<10
    m=checkTime(m);
    document.getElementById('modeline-time').innerHTML="UTC "+h+":"+m;
    t=setTimeout('startTime()',500);
};

function checkTime(i)
{
    if (i<10)
    {
        i="0" + i;
    }
    return i;
};

startTime();

var modelineTime = document.getElementById('modeline-time');

modelineTime.addEventListener("mouseover", toogleMinibufferTime);
modelineTime.addEventListener("mouseout", toogleMinibuffer);

function toogleMinibufferTime(){
    var minibuffer = document.querySelector(".minibuffer");
    const days = ["dom", "lun", "mar", "mie", "jue", "vie", "sab"];
    const months = ["jan", "feb", "mar", "apr", "may", "jun", "july", "aug", "sep", "oct", "nov", "dec"];

    const today=new Date();
    let day_sem = days[today.getDay()];
    let month = months[today.getMonth()];
    let day_month = today.getDate();
    let year = today.getFullYear();
    minibuffer.innerHTML = '<p>'+ day_sem + ' ' + month + ' ' + day_month + ', ' +  year +'</p>';
};
